import { Funscript } from '@ohdoki/handy-sdk';

const SCRIPT: Funscript = {
  actions: [
    { at: 0, pos: 0 },
    { at: 1000, pos: 100 },
    { at: 2000, pos: 0 },
    { at: 3000, pos: 100 },
    { at: 4000, pos: 0 },
    { at: 5000, pos: 100 },
    { at: 6000, pos: 0 },
    { at: 7000, pos: 100 },
    { at: 8000, pos: 0 },
    { at: 9000, pos: 100 },
    { at: 10000, pos: 0 },
  ],
};

export default SCRIPT;
