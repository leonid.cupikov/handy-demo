import { createContext, useState } from 'react';
import Handy, { ConnectionKey, Script } from '@ohdoki/handy-sdk';

type Response = { success: boolean } | { error: string };

interface Context {
  keys: ConnectionKey[];
  addHandy: (key: ConnectionKey) => Promise<Response>;
  removeHandy: (key: ConnectionKey) => Response;
  uploadScript: (script: Script) => Promise<Response>;
  playScript: (
    startTime?: number,
    connectionKey?: ConnectionKey,
  ) => Promise<Response>;
  stop: () => Promise<Response>;
  loop: (activated: boolean) => Promise<Response>;
  resetPosition: () => Promise<Response>;
}

export const HandiesContext = createContext<Context>({
  keys: [],
  addHandy: async () => ({ success: true }),
  removeHandy: () => ({ success: true }),
  uploadScript: async () => ({ success: true }),
  playScript: async () => ({ success: true }),
  stop: async () => ({ success: true }),
  loop: async () => ({ success: true }),
  resetPosition: async () => ({ success: true }),
});

const HANDY = new Handy();

const Handies: React.FC = ({ children }) => {
  const [keys, setKeys] = useState<ConnectionKey[]>([]);

  const localResetPosition = () =>
    HANDY.withMode(Handy.Mode.HDSP, key =>
      HANDY.API.put.HDSP.xpvp(key, {
        stopOnTarget: true,
        position: 0,
        velocity: 100,
      }),
    );

  const runPromise = (fn: () => Promise<unknown>, name: string) => {
    return fn()
      .then(() => ({ success: true }))
      .catch(() => ({ error: `Failed to run ${name}` }));
  };

  const addHandy: Context['addHandy'] = async connectionKey => {
    if (keys.includes(connectionKey)) return { success: true };
    return HANDY.connect(connectionKey)
      .then(({ result }) => {
        if (result === -1) throw new Error();
        return HANDY.setStroke({ min: 0, max: 100 });
      })
      .then(localResetPosition)
      .then(() => {
        setKeys(_keys => [..._keys, connectionKey]);
        return { success: true };
      })
      .catch(() => ({ error: `Failed to connect ${connectionKey}` }));
  };

  const removeHandy: Context['removeHandy'] = connectionKey => {
    if (!keys.includes(connectionKey)) {
      return { error: `Handy with ${connectionKey} key not connected` };
    }

    HANDY.disconnect(connectionKey);
    setKeys(list => list.filter(k => k !== connectionKey));
    return { success: true };
  };

  const uploadScript: Context['uploadScript'] = script => {
    return runPromise(() => HANDY.uploadScript(script), 'uploadScript');
  };

  const playScript: Context['playScript'] = (startTime, connectionKey) => {
    return runPromise(async () => {
      await HANDY.getServerLatency();
      await HANDY.playScript(startTime, connectionKey);
    }, 'setStroke');
  };

  const stop: Context['stop'] = () => {
    return runPromise(
      () =>
        HANDY.withMode(Handy.Mode.HSSP, connectionKey => {
          HANDY.API.put.HSSP.stop(connectionKey);
        }),
      'stop',
    );
  };

  const loop: Context['loop'] = activated => {
    return runPromise(
      () =>
        HANDY.withMode(Handy.Mode.HSSP, connectionKey => {
          HANDY.API.put.HSSP.loop(connectionKey, { activated });
        }),
      'loop',
    );
  };

  const resetPosition: Context['resetPosition'] = () => {
    return runPromise(localResetPosition, 'resetPosition');
  };

  return (
    <HandiesContext.Provider
      value={{
        keys,
        addHandy,
        removeHandy,
        uploadScript,
        playScript,
        stop,
        loop,
        resetPosition,
      }}
    >
      {children}
    </HandiesContext.Provider>
  );
};

export default Handies;
