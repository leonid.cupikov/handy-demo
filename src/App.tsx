import { useContext, useEffect, useRef, useState } from 'react';

import './App.scss';

import { HandiesContext } from './context/handies';
import SCRIPT from './script';

const LOOPED_SCRIPT = JSON.parse(JSON.stringify(SCRIPT));
LOOPED_SCRIPT.actions.pop();

const App: React.FC = () => {
  const timer = useRef(0);
  const currentTime = useRef(0);
  const lastUpdateTime = useRef(0);
  const duration = useRef(SCRIPT.actions[SCRIPT.actions.length - 1].at);

  const [step, setStep] = useState<1 | 2 | 3>(1);
  const [looped, setLooped] = useState(false);

  const {
    keys,
    addHandy,
    removeHandy,
    uploadScript,
    playScript,
    stop,
    loop,
    resetPosition,
  } = useContext(HandiesContext);

  const [key, setKey] = useState('');
  const [loading, setLoading] = useState(false);

  const Button: React.FC<React.ButtonHTMLAttributes<HTMLButtonElement>> = ({
    onClick,
    ...params
  }) => {
    return (
      <button
        {...params}
        disabled={loading}
        onClick={
          onClick
            ? async event => {
                setLoading(true);
                await onClick(event);
                setLoading(false);
              }
            : void 0
        }
      >
        {loading ? 'Loading...' : params.title}
      </button>
    );
  };

  async function play(gap: number) {
    return new Promise(resolve => {
      let counter = keys.length;
      keys.forEach((connectionKey, i) => {
        setTimeout(async () => {
          await playScript(
            currentTime.current % duration.current,
            connectionKey,
          );
          counter--;
          if (!counter) resolve(true);
        }, (gap * i) % duration.current);
      });
    });
  }

  async function pause() {
    currentTime.current = currentTime.current % duration.current;
    lastUpdateTime.current = 0;
    cancelAnimationFrame(timer.current);
    await stop();
  }

  async function localStop() {
    currentTime.current = 0;
    lastUpdateTime.current = 0;
    cancelAnimationFrame(timer.current);
    await stop();
  }

  function update() {
    timer.current = requestAnimationFrame(async ct => {
      if (!duration) return;
      if (!lastUpdateTime.current) lastUpdateTime.current = ct;

      currentTime.current = Math.round(
        currentTime.current + ct - lastUpdateTime.current,
      );

      if (!looped && currentTime.current >= duration.current) {
        return localStop();
      }

      lastUpdateTime.current = ct;
      update();
    });
  }

  function renderLoopedButton(newLooped: boolean) {
    return (
      <Button
        key={'' + newLooped}
        title={`Prepare${newLooped ? ' looped' : ''}`}
        onClick={async () => {
          if (newLooped) await pause();
          else {
            await localStop();
            await resetPosition();
          }

          await loop(newLooped);
          await uploadScript({
            json: newLooped ? LOOPED_SCRIPT : SCRIPT,
          });
          setLooped(newLooped);
          setStep(3);
        }}
      />
    );
  }

  useEffect(() => {
    if (!keys.length) setStep(1);
  }, [keys]);

  return (
    <div className="App">
      <h1>Step 1: Add keys</h1>
      <ul>
        {keys.map(k => (
          <li key={k}>
            {k}
            <Button
              title="Delete"
              onClick={() => {
                const res = removeHandy(k);
                if ('error' in res) return alert(res.error);
              }}
            />
          </li>
        ))}
      </ul>
      <form
        onSubmit={async e => {
          e.preventDefault();
          if (!key.trim()) return;

          setLoading(true);
          const res = await addHandy(key);
          setLoading(false);

          if ('error' in res) return alert(res.error);
          setKey('');
          setStep(2);
        }}
      >
        <input value={key} onChange={e => setKey(e.target.value)} />
        <Button title="Add" type="submit" />
      </form>

      {step >= 2 && (
        <>
          <h1>Step 2: Prepare</h1>
          {renderLoopedButton(false)}
          {renderLoopedButton(true)}
          <br />
          Looped: {looped.toString()}
        </>
      )}

      {step >= 3 && (
        <>
          <h1>Step 3: Play</h1>
          {[0, 250, 500, 1000].map(ms => (
            <Button
              key={'' + ms}
              title={`${ms / 1000}s`}
              onClick={async () => {
                cancelAnimationFrame(timer.current);
                await play(ms);
                update();
              }}
            />
          ))}
          <Button
            title="Stop"
            onClick={() => localStop().then(resetPosition)}
          />
          <Button title="Pause" onClick={pause} />
        </>
      )}
    </div>
  );
};

export default App;
