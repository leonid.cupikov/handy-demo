import ReactDOM from 'react-dom';

import './index.scss';

import App from './App';
import Handies from './context/handies';

ReactDOM.render(
  <Handies>
    <App />
  </Handies>,
  document.getElementById('root'),
);
